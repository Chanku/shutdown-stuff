#include <unistd.h>
#include <sys/reboot.h>

/*
 * The main purpose of this is to restart the computer, until I can actually
 * 	write a wrapper for Python, this will have to do.
 * This works by calling sync and then telling the kernal to reboot. 
 */
int main(){
	sync();
	reboot(RB_AUTOBOOT);
	return 0;
}
