#include <unistd.h>
#include <sys/reboot.h>

/*
 * The main purpose of this is to halt the computer, until I can actually
 * 	write a wrapper in for Python, this will have to do.
 * This works by calling sync and then telling the kernal to halt. 
 */
int main(){
	sync();
	reboot(RB_HALT_SYSTEM);
	return 0;
}
