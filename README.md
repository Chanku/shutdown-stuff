#Shutdown Stuff (SS)

####What is Shutdown Stuff?
Well it's merely a collection of C files/programs that allows me to shutdown and manage the system. 

####Installation  
Simply run the following:  
gcc -o sdown yai\_shutdown.c  
gcc -o rboot yai\_reboot.c  
gcc -o halt yai\_halt.c  

Then put them where Yet Another Init can find them. By defauly it looks inside of /etc/yai/

####Requirements  
It requires the following:
- GCC or another C Compiler

##PFAQ (Possibly Frequently Asked Questions)

####Why split this from the Yet Another Init?  
Because of the nature of the init, I felt that having this in there was unnecessary bloat. Especially since someone else may want to use these for something.


####Where can I get Yet Another Init
You can get it [here](http://bitbucket.org/Chanku/yet-another-init)
