#include <unistd.h>
#include <sys/reboot.h>

/*
 * The main purpose of this is to shutdown the computer, until I can actually
 * 	write a wrapper for this in Python, this will have to do.
 * This works by calling sync and then telling the kernal to shut off. 
 */
int main(){
	sync();
	reboot(RB_POWER_OFF);
	return 0;
}
